package showmessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ContentController {

	@Autowired
	private SimpMessagingTemplate smt;
    
    @RequestMapping(value = "/send", method = RequestMethod.GET)
    @ResponseBody
    public Content broadcast(@RequestParam("id") String id,
                       @RequestParam("data") String data) {
	    smt.convertAndSend("/topic/messages", new Content(id + ": " + data));
        return new Content( id + ": " + data);
    }

}
